# IDE设置

**适用于 Rust 内源发行版 1.78.0 及之前的版本安装。** 从 1.80.1 版本起无需单独设置 IDE。

目前官方支持的编辑器[参见官网](https://www.rust-lang.org/tools)。

**1**.安装IDE。

官方支持多个IDE，本文以社区 VS Code 和内部 VSCode-huawei/WeCode 为例，选择安装一个即可。

（1）安装 VS Code。
下载并安装适用于 Windows 的 [Visual Studio Code](https://code.visualstudio.com/)。

（2）安装 VSCode-huawei/WeCode。
两个工具都是公司内部基于VS Code开发的，用户界面、使用方法、应用市场等相同，唯一区别是WeCode预安装了一些插件。可以通过[工具市场](http://his.huawei.com/portal/#/search.html?searchvalue=VSCode)安装。
![VSCode-huawei-2](../image/tool_market.png)



**2**.安装对应IDE的插件。

（1）安装 VS Code 对应插件。

- 安装 CodeLLDB 扩展。 可以从 [Visual Studio Marketplace 安装 CodeLLDB 扩展](https://marketplace.visualstudio.com/items?itemName=vadimcn.vscode-lldb)，也可以打开 VS Code，然后在“扩展”菜单 (Ctrl+Shift+X) 中搜索“CodeLLDB”。

> 若在安装 **Rust 内源发行版** 之前安装过 VS Code, 则内源发行版的安装程序会为其自动安装 `rust-analyzer` 插件，无需手动安装。否则需要在 VS Code 的 “扩展”菜单中手动搜索安装 `rust-analyzer`。

（2）安装 VSCode-huawei/WeCode 对应插件。
打开 VSCode-huawei/WeCode，然后在“扩展”菜单中搜索安装Rust插件：Native Debug、CodeLLDB(调试)和rust-analyzer(代码跳转和联想)。

![VSCode-huawei-1](../image/wecode_tool.png)


**【参考链接】**

其他详细安装信息，参考：
https://docs.microsoft.com/zh-cn/windows/dev-environment/rust/setup

https://marketplace.visualstudio.com/items?itemName=rust-lang.rust

   