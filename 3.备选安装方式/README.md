### FAQ

1. **Linux 中按照步骤安装过后显示安装成功，但是 `rustc -V` 显示找不到命令**。

    答：确保在命令行中输入 `source "$HOME/.cargo/env"`，或者重启当前 shell。

2. **（Windows 离线安装）手动下载的 MSVC 生成工具包 (msvc-buildtools-with-sdk.tar.gz) 解压下来之后没有看到有文件夹**。

    答：一些解压工具 (例如 `7-zip`) 无法直接解压 `.tar.gz` 格式的压缩包，会将其先解压成 `.tar` 文件。需要再次将此 `.tar` 文件解压一次才算成功。

3. **（Windows）安装 MSVC 生成工具时提示 `发生未知错误，安装失败`**。

    答: 个别情况下 Windows 在安装完 MSVC 生成工具之后需要重启，建议重启电脑之后再尝试运行 `rustup-init.exe` 继续安装 Rust。

    若重启后问题中的告警仍然出现, 则尝试 [手动下载 MSVC 生成工具离线包]() 并解压，在解压后的目录使用以下命令启动 `vs_buildtools.exe` 安装生成工具:

    ```PowerShell
    vs_buildtools.exe --noweb --wait --focusedUi --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 --add Microsoft.VisualStudio.Component.Windows11SDK.22000
    ```

    之后再运行 `rustup-init.exe` 继续安装 Rust.
