
## 5. 案例


Rust 是一门注重安全（safety）、速度（speed）和并发（concurrency）的现代系统编程语言。Rust 通过内存安全来实现以上目标，但不使用垃圾回收机制（garbage collection, GC）。

本章节通过创建案例来展示 Rust 的内存安全特性和线程安全特性，希望对理解 Rust 的内部机制提供帮助。